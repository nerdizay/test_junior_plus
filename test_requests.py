import requests
from logger import logger

URL = "http://127.0.0.1:8888/get_form"

jsons = [
    {
        "name": "send sms",
        "phone_receiver": "+7 953 808 90 65",
        "message": "Hi! How are you??"
    },
    {
        "name": "send sms",
        "phone_receiver": "+7 953 808 90 65",
    },
    {
        "name": "send sms",
        "phone_receiver": "+7 953 808 90065",
        "message": "Hi! How are you??"
    },
    {
        "name": "send smSS",
        "phone_receiver": "+7 953 808 90 65",
        "message": "Hi! How are you??"
    },
    {
        "name": "send sms",
        "phone_receiver": "+7 953 808 90 65",
        "message": "2017-09-15"
    },
    {
        "name": "send sms",
        "phone_receiver": "+7 953 808 90 65",
        "message": 25
    },
    {
        "name": "registation",
        "login": "some_login",
        "password": "some_password",
        "email": "nerdizay@gmail.com",
        "phone_number": "+7 952 803 08 33" 
    },
    {
        "name": "registation",
        "login": "some_login",
        "email": "nerdizay@gmail.com",
        "phone_number": "+7 952 803 08 33" 
    },
    {
        "name": "registation",
        "login": "some_login",
        "password": "some_password",
        "email": "nerdizay@gmailcom",
        "phone_number": "+7 952 803 08 33" 
    },
    {
        "name": "regista",
        "login": "some_login",
        "password": "some_password",
        "email": "nerdizay@gmail.com",
        "phone_number": "+7 952 803 08 33" 
    },
    {
        "name": "registation",
        "login": "some_login",
        "password": "some_password",
        "email": "nerdizay@gmail.com",
        "phone_number": "+8 952 803 08 33" 
    },
    {
        "name": "ivite to a wedding",
        "date_wedding": "2017-10-15",
        "invite_message": "Приглашаю Вас на свадьбу!"
    },
    {
        "name": "ivite to a wedding",
        "invite_message": "Приглашаю Вас на свадьбу!"
    },
    {
        "name": "ivite to a we",
        "date_wedding": "2017-10-15",
        "invite_message": "Приглашаю Вас на свадьбу!"
    },
    {
        "name": "ivite to a we",
        "date_wedding": "2017-10-156",
        "invite_message": "Приглашаю Вас на свадьбу!"
    },
    {
        "name": "ivite to a we",
        "date_wedding": "2017-10-156",
        "invite_message": None
    },
    {
        "name": "send email",
        "from": "nerdizay@gmail.com",
        "to": "some_email@gmail.com",
        "body": "Здравствуйте, я по поводу вакансии...",
        "title": "Вакансия"
    },
    {
        "name": "send email send",
        "from": "nerdizay@gmail.com",
        "to": "some_email@gmail.com",
        "body": "Здравствуйте, я по поводу вакансии...",
        "title": "Вакансия"
    },
    {
        "name": "send email",
        "from": "nerdizaygmail.com",
        "to": "some_email@gmail.com",
        "body": "Здравствуйте, я по поводу вакансии...",
        "title": "Вакансия"
    },
    {
        "name": "send email",
        "from": "nerdizay@gmail.com",
        "to": "some_emailgmail.com",
        "body": "Здравствуйте, я по поводу вакансии...",
        "title": "Вакансия"
    },
    {
        "name": "send email",
        "from": "nerdizay@gmail.com",
        "to": "some_email@gmail.com",
        "body": "Здравствуйте, я по поводу вакансии...",
    },
]

for request in jsons:
    response = requests.post(URL, json=request)
    log = f"\n{request = }\n{response.status_code = }\n{response.text = }"
    if response.status_code == 200:
        logger.info(log)
    else:
        logger.error(log)