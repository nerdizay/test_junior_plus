from tinydb import TinyDB
db = TinyDB('db.json')

if __name__ == "__main__":
    db.insert_multiple([
        {
            'name': 'registation',
            'login': 'text',
            'password': 'text',
            'email': 'email',
            'phone_number': 'phone',
        },
        {
            'name': 'send email',
            'from': 'email',
            'to': 'email',
            'body': 'text',
            'title': 'text',
        },
        {
            'name': 'send sms',
            'phone_receiver': 'phone',
            'message': 'text'
        },
        {
            'name': 'ivite to a wedding',
            'date_wedding': 'date',
            'invite_message': 'text'
        }
    ])
