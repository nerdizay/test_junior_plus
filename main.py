import uvicorn
import json
from fastapi import Depends, FastAPI, Request, Header
from fastapi.middleware.cors import CORSMiddleware
from utils import get_form, response_json_with_correct_date_async

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post('/get_form')
@response_json_with_correct_date_async
async def get_form_wrapper(request: Request):
    request = await request.body()
    request = json.loads(request)
    print(request)
    response, status_code = get_form(request)
    return response, status_code


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8888, reload=True)