from functools import wraps
import json
from json import JSONEncoder
from datetime import date, datetime
from fastapi import Response
from db import db
from tinydb import Query, where
import re
from pydantic import BaseModel, EmailStr, Field
        
        
class Email(BaseModel):
    email: EmailStr
    

class DateTimeEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, (date, datetime)):
            return obj.isoformat()
        # return JSONEncoder.default(self, obj)


def response_json_with_correct_date_async(f):
    @wraps(f)
    async def new_f(*args, **kwargs):
        
        response = await f(*args, **kwargs)
        
        if type(response) == tuple and len(response) == 2:
            response, status_code = response[0], response[1]
        elif type(response) in [dict, list]:
            status_code = 200
        else:
            raise TypeError
            
        response = json.dumps(
            response,
            cls=DateTimeEncoder,
            ensure_ascii=False
        )
        return Response(
            content=response,
            status_code=status_code,
            headers={"content-type": "application/json"}
        )
    return new_f


def define_type(value):
    try:
        datetime.strptime(value, '%d.%m.%Y')
        return "date"
    except ValueError:
        pass
    except TypeError:
        return "unknown"
    try:
        datetime.fromisoformat(value)
        return "date"
    except ValueError:
        pass
    except TypeError:
        return "unknown"
    
    phone = r'\+7 [0-9]{3,3} [0-9]{3,3} [0-9]{2,2} [0-9]{2,2}'
    if re.fullmatch(phone, value) is not None:
        return "phone"
    try:
        Email(**{'email': value})
        return "email"
    except Exception as e:
        pass
        
    if type(value) == str:
        return "text"
    
    return "unknown"
    

def get_form(request: dict) -> tuple[dict, int]:
    try:
        name = request.pop('name')
    except KeyError:
        name = None
        status_code = 400
    else:
        status_code = 200
        
    fields_and_types_fields_request = {}
    for field, value in request.items():
        fields_and_types_fields_request[field] = define_type(value)
        
    if status_code == 400:
        return fields_and_types_fields_request, status_code
        
    forms = db.search(where('name') == name)
    if not forms:
        return fields_and_types_fields_request, 400
    
    form = dict(forms[0])
    print(form)
    for field, value in fields_and_types_fields_request.items():
        try:
            if form.pop(field) != value:
                return fields_and_types_fields_request, 400
        except KeyError:
            return fields_and_types_fields_request, 400
        
    if len(form) == 1:
        return form, 200
        
    return fields_and_types_fields_request, 400
            